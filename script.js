// document.addEventListener('click', () => {
//     document.querySelector('#popup').style.display = 'block';
// });

document.querySelector('#close-popup').addEventListener('click', (event) => {
    event.stopPropagation();
    event.currentTarget.parentElement.style.display = "none";
});

document.addEventListener('scroll', (event) => {
    if (window.scrollY > 200) document.querySelector('#popup').style.display = 'block';
});

window.addEventListener('load', () => {
    let timer = 5000;
    setTimeout(() => {
        if (document.querySelector('#popup').style.display != 'block') document.querySelector('#popup').style.display = 'block';
    }, timer);
});